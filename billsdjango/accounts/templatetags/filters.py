from django import template

register = template.Library()

@register.filter(name='addCls')
def addCls(value, arg):
    return value.as_widget(attrs={'class': arg})