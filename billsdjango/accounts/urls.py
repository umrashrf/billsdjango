from django.conf.urls import patterns, include, url
from django.contrib.auth.views import *

from . import views


urlpatterns = patterns('',
    url(r'^logout/$', logout, {'next_page': '/'}, name="logout"),
    url(r'^register/$', views.BillsRegistrationView.as_view(), name="registration_register"),
    url(r'', include('django.contrib.auth.urls')),
    url(r'', include('registration.backends.simple.urls')),
)