import pytz

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView

from registration.backends.simple.views import RegistrationView

from . import LoginRequiredMixin


class BillsRegistrationView(RegistrationView):
    def get_success_url(self, request, user):
        return (reverse('index'), (), {})


class ProfileView(LoginRequiredMixin, DetailView):
    title = 'View Profile'
    template_name = 'profile.html'

    def get_object(self, queryset=None):
        if not queryset:
            queryset = User.objects.all()
        return queryset.get(id=self.request.user.id)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['title'] = self.title
        context['timezones'] = pytz.common_timezones
        return context
