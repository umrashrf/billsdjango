import datetime

from dateutil import tz

from django.http import HttpResponse
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


def get_utc5_date(dt):
    utc5 = tz.tzoffset('UTC', 5*60*60) # UTC+5
    return dt.replace(tzinfo=utc5)

def get_utc5_now():
    utc5 = tz.tzoffset('UTC', 5*60*60) # UTC+5
    return datetime.datetime.utcnow().replace(tzinfo=utc5)

def get_epoch_time(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

def get_epoch_time_millis(dt):
    return get_epoch_time(dt) * 1000.0

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

class JSONResponseMixin(TemplateView):
    def get_json_response(self, content, **httpresponse_kwargs):
        return HttpResponse(content, content_type='application/json',
                            **httpresponse_kwargs)

    def render_to_response(self, context):
        return self.get_json_response("{}")
