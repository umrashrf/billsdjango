from django import forms
from django.contrib.auth.models import User

from . import models


class AddConnection(forms.Form):
    cities = {}
    for branch in models.Branch.objects.all():
        city_name = branch.city.long_name
        if city_name not in cities:
            cities[city_name] = []
        cities[city_name].append((branch.id, branch))
    combo = [(city, tuple(branches)) for city, branches in cities.iteritems()]
    branch = forms.ChoiceField(choices=combo)


class AddCompanyConnection(forms.Form):
    user = forms.IntegerField(widget=forms.HiddenInput())

    def __init__(self, branch=None, *args, **kwargs):
        super(AddCompanyConnection, self).__init__(*args, **kwargs)
        self.branch = branch
        branch_obj = models.Branch.objects.get(pk=self.branch)
        branch_params = models.BranchParameters.objects.filter(branch=branch_obj)
        for bparam in branch_params:
            self.fields[bparam.key] = forms.CharField(label=bparam.value,
                                                        help_text=bparam.comments,
                                                        required=True)

    def clean(self):
        cleaned_data = super(AddCompanyConnection, self).clean()
        user_id = cleaned_data['user']

        try:
            User.objects.get(id=user_id)
        except:
            raise forms.ValidationError("User does not exists.")

        branch_obj = models.Branch.objects.get(pk=self.branch)
        branch_params = models.BranchParameters.objects.filter(branch=branch_obj)

        # check if connection exists already for any user
        key_value_matches = 0
        for bparam in branch_params:
            value = cleaned_data.get(bparam.key)
            try:
                models.ConnectionParameters.objects.get(key=bparam.key, value=value)
                key_value_matches += 1
            except:
                pass

        if len(branch_params) == key_value_matches:
            # record already exists
            raise forms.ValidationError("This connection already exists.")

        return cleaned_data


class ShareConnection(forms.Form):
    email = forms.CharField(max_length=150)
