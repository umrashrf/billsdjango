import pprint

from django import forms
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

from billsdjango import settings
from .models import (City, Company, Branch, BranchParameters,
                    Connection, ConnectionParameters, Bill, Units)

if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_mail
else:
    from django.core.mail import send_mail


class ConnectionParametersForm(forms.ModelForm):
    key = forms.CharField(widget=forms.TextInput(
                                    attrs={
                                        #'disabled': 'disabled', 'readonly': 'True',
                                        #'style': 'background:rgba(0,0,0,0); border: 1px solid rgba(0,0,0,0);'
                                    }))

class ConnectionParametersInline(admin.StackedInline):
    model = ConnectionParameters
    form = ConnectionParametersForm
    max_num = 2

class ConnectionAdmin(admin.ModelAdmin):
    inlines = [
        ConnectionParametersInline,
    ]
    list_display = ('id', 'user', 'branch')


class BillTabular(admin.ModelAdmin):
    list_display = ('id', 'billing_date', 'customer_name', 'dues', 'connection')


admin.site.register(City)
admin.site.register(Company)
admin.site.register(Branch)
admin.site.register(BranchParameters)
admin.site.register(Connection, ConnectionAdmin)
admin.site.register(ConnectionParameters)
admin.site.register(Bill, BillTabular)
admin.site.register(Units)


class UserAdmin(admin.ModelAdmin):
    actions = ['welcome_user', 'missing_connection_reminder']

    def welcome_user(modeladmin, request, queryset):
        for user in queryset:
            newpassword = User.objects.make_random_password()
            user.set_password(newpassword)
            user.save()
            data = {
                'firstname': user.first_name,
                'username': user.username,
                'password': newpassword,
            }
            message = ''
            tpl = '%s/%s' % (settings.BASE_DIR, 'reports/templates/emails/welcome_new_user.txt')
            with open(tpl, 'r') as f:
                message = f.read()
            send_mail('Welcome to Bills.pk', message % data, settings.DEFAULT_FROM_EMAIL,
                        [user.email], fail_silently=True)

    welcome_user.short_description = "Send welcome email to selected users"

    def missing_connection_reminder(modeladmin, request, queryset):
        for user in queryset:
            data = dict(
                username=user.first_name or user.username,
            )
            subject = 'Add more connections'
            message = ''
            tpl = '%s/%s' % (settings.BASE_DIR, 'reports/templates/emails/missing_connection_reminder.txt')
            with open(tpl, 'r') as f:
                message = f.read()
            send_mail(subject, message % data, settings.DEFAULT_FROM_EMAIL,
                        [user.email], fail_silently=False)

    missing_connection_reminder.short_description = "Send reminder to add more connections"


admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return pprint.pformat(obj.get_decoded()).replace('\n', '<br>\n')
    _session_data.allow_tags = True

    list_display = ['session_key', '_session_data', 'expire_date']
    readonly_fields = ['_session_data']
    exclude = ['session_data']

admin.site.register(Session, SessionAdmin)
