from __future__ import division

import json
import time

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from .. import (get_utc5_now, get_epoch_time_millis,
                LoginRequiredMixin, JSONResponseMixin)
from .. import models


class DashboardChart(LoginRequiredMixin, JSONResponseMixin):
    def render_to_response(self, context):
        user = User.objects.get(id=self.request.user.id)
        connections = models.Connection.objects.filter(Q(user=user) | Q(shared_with=user)).distinct()

        utc_now = datetime.strptime(get_utc5_now().strftime('%Y-%m'), '%Y-%m')

        bills = models.Bill.objects.filter(Q(connection__user=user) | Q(connection__shared_with=user))
        if bills:
            latest_bill = bills.latest('billing_date')
            if latest_bill:
                utc_now = latest_bill.billing_date

        end_date = utc_now
        start_date = end_date - relativedelta(months=11)

        plots = []
        overall_data = {}
        for connection in connections:
            series = []
            bills = (models.Bill.objects.filter(connection=connection,
                                                billing_date__range=[start_date, end_date])
                                        .order_by('-billing_date'))
            for bill in bills:
                dt = datetime.strptime(bill.billing_date.strftime('%Y-%m'), '%Y-%m')
                milli = get_epoch_time_millis(dt)
                if milli not in overall_data:
                    overall_data[milli] = []
                overall_data[milli].append(float(bill.dues))
                series.append([milli, float(bill.dues)])
            series.sort(key=lambda x: x[0])
            try:
                plots.append({
                    'name': '%s - %s' % (connection.branch.company.short_name,
                                        connection.bill_set.last().customer_name.strip('.')),
                    'data': series,
                    'visible': False
                })
            except:
                pass

        overall = []
        for dtime, ddues in overall_data.iteritems():
            overall.append([dtime, sum(ddues)])
        if overall:
            overall.sort(key=lambda x: x[0])
            plots.append({
                'name': 'Overall',
                'data': overall
            })

        return self.get_json_response(json.dumps(plots))


class BillDetailsChart(LoginRequiredMixin, JSONResponseMixin):
    def render_to_response(self, context):
        selected_bill = None

        bill = models.Bill.objects.get(pk=self.kwargs.get('bill_id'))
        if bill.connection.user.id == self.request.user.id:
            selected_bill = bill
        elif bill.connection.shared_with.filter(pk=self.request.user.id).exists():
            selected_bill = bill

        try:
            latest_bill = (models.Bill.objects.filter(connection=selected_bill.connection)
                                                .latest('billing_date'))
        except:
            latest_bill = None

        last_year_bills = []
        if latest_bill:
            end_date = latest_bill.billing_date
            start_date = end_date - relativedelta(months=11)
            last_year_bills = (models.Bill.objects.filter(connection=selected_bill.connection,
                                                            billing_date__range=[start_date, end_date])
                                                    .order_by('-billing_date'))

        plots = []
        data = {}
        for bill in last_year_bills:
            dt = datetime.strptime(bill.billing_date.strftime('%Y-%m'), '%Y-%m')
            milli = get_epoch_time_millis(dt)
            if milli not in data:
                data[milli] = []
            data[milli].append(float(bill.amount or bill.dues))

        series = []
        for dtime, damounts in data.iteritems():
            series.append([dtime, sum(damounts)])
        series.sort(key=lambda x: x[0])
        plots.append({
            'name': '%s - %s' % (latest_bill.connection.branch.company.short_name,
                                latest_bill.customer_name.strip('.')),
            'data': series
        })

        return HttpResponse(json.dumps(plots), content_type="application/json")
