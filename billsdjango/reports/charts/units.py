import json

from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.db.models import Q
from django.contrib.auth.models import User
from django.views.generic import TemplateView

from .. import models
from .. import (get_utc5_now, get_epoch_time_millis,
                LoginRequiredMixin, JSONResponseMixin)


class Dashboard(LoginRequiredMixin, JSONResponseMixin):

    def render_to_response(self, context):
        user = User.objects.get(id=self.request.user.id)
        companies = models.Company.objects.filter(short_name__in=['k-electric', 'ssgc'])
        connections = models.Connection.objects.filter(Q(branch__company__in=companies),
                                                         Q(user=user) | Q(shared_with=user)) \
                                                .distinct()

        utc_now = datetime.strptime(get_utc5_now().strftime('%Y-%m'), '%Y-%m')

        bills = models.Bill.objects.filter(Q(connection__branch__company__in=companies),
                                            Q(connection__user=user) | Q(connection__shared_with=user))
        if bills:
            latest_bill = bills.latest('billing_date')
            if latest_bill:
                utc_now = latest_bill.billing_date

        end_date = utc_now
        start_date = end_date - relativedelta(months=11)

        plots = []
        for connection in connections:
            series = []
            bills = (models.Bill.objects.filter(connection=connection,
                                                billing_date__range=[start_date, end_date])
                                        .order_by('-billing_date'))
            for bill in bills:
                units = 0
                if hasattr(bill, 'units'):
                    units = bill.units.billable_units_computed
                dt = datetime.strptime(bill.billing_date.strftime('%Y-%m'), '%Y-%m')
                milli = get_epoch_time_millis(dt)
                series.append([milli, units])
            series.sort(key=lambda x: x[0])
            plots.append({
                'name': '%s - %s' % (connection.branch.company.short_name,
                                    connection.bill_set.last().customer_name.strip('.')),
                'data': series
            })

        return self.get_json_response(json.dumps(plots))


class BillUnits(LoginRequiredMixin, JSONResponseMixin):
    def render_to_response(self, context):
        selected_bill = None

        bill = models.Bill.objects.get(pk=self.kwargs.get('bill_id'))
        if bill.connection.user.id == self.request.user.id:
            selected_bill = bill
        elif bill.connection.shared_with.filter(pk=self.request.user.id).exists():
            selected_bill = bill

        if selected_bill.connection.branch.company.short_name.lower() not in ['k-electric', 'ssgc']:
            return

        try:
            latest_bill = (models.Bill.objects.filter(connection=selected_bill.connection)
                                                .latest('billing_date'))
        except:
            latest_bill = None

        last_year_bills = []
        if latest_bill:
            end_date = latest_bill.billing_date
            start_date = end_date - relativedelta(months=11)
            last_year_bills = (models.Bill.objects.filter(connection=selected_bill.connection,
                                                            billing_date__range=[start_date, end_date])
                                                    .order_by('-billing_date'))

        plots = []
        data = {}
        for bill in last_year_bills:
            dt = datetime.strptime(bill.billing_date.strftime('%Y-%m'), '%Y-%m')
            milli = get_epoch_time_millis(dt)
            if milli not in data:
                data[milli] = []
            if hasattr(bill, 'units'):
                data[milli].append(bill.units.billable_units_computed)

        series = []
        for dtime, dunits in data.iteritems():
            series.append([dtime, sum(dunits)])
        series.sort(key=lambda x: x[0])
        plots.append({
            'name': '%s - %s' % (latest_bill.connection.branch.company.short_name,
                                latest_bill.customer_name.strip('.')),
            'data': series
        })

        return self.get_json_response(json.dumps(plots))

