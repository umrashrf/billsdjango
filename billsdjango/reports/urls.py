from django.conf.urls import patterns, url
from django.views.generic.base import RedirectView

from . import views
from . import charts
from .views import units
from .views.changes import ChangesView
from .charts import units as UnitCharts
from billsdjango.accounts.views import ProfileView


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='dashboard/', permanent=False), name='index'),
    url(r'^dashboard/$', views.DashboardView.as_view(), name='dashboard'),
    url(r'^connections/$', views.ConnectionsList.as_view(), name='connections'),
    url(r'^connections/(?P<connection_id>\d+)/share/$', views.ShareConnection.as_view(), name='share_connection'),
    url(r'^connections/new/$', views.AddConnection.as_view(), name='add_connection'),
    url(r'^connections/(?P<branch>\d+)/$', views.AddCompanyConnection.as_view(), name='add_company_connection'),
    url(r'^bills/(?P<bill_id>\d+)/details/$', views.ViewBillDetails.as_view(), name='view_bill_details'),

    url(r'^units/$', units.Dashboard.as_view(), name='units'),
    url(r'^bills/(?P<bill_id>\d+)/units/$', units.BillUnits.as_view(), name='bill_units'),

    url(r'^bills/changes/$', ChangesView.as_view(), name='changes'),
    url(r'^bills/(?P<conn_id>\d+)/changes/$', ChangesView.as_view(), name='bill_changes'),

    url(r'^bills/charts/current_bills/$', charts.DashboardChart.as_view(), name='dashboard_chart'),
    url(r'^bills/charts/(?P<bill_id>\d+)/details/$', charts.BillDetailsChart.as_view(), name='bill_details_chart'),

    url(r'^units/charts/dashboard/$', UnitCharts.Dashboard.as_view(), name='units_chart'),
    url(r'^bills/(?P<bill_id>\d+)/units/charts$', UnitCharts.BillUnits.as_view(), name='bill_units_chart'),

    url(r'^profile/$', ProfileView.as_view(), name='profile')
)
