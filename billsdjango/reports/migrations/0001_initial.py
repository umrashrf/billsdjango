# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'City'
        db.create_table(u'reports_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('short_name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('long_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'reports', ['City'])

        # Adding model 'Company'
        db.create_table(u'reports_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('short_name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('long_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'reports', ['Company'])

        # Adding model 'Branch'
        db.create_table(u'reports_branch', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.City'])),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Company'])),
        ))
        db.send_create_signal(u'reports', ['Branch'])

        # Adding unique constraint on 'Branch', fields ['city', 'company']
        db.create_unique(u'reports_branch', ['city_id', 'company_id'])

        # Adding model 'BranchParameters'
        db.create_table(u'reports_branchparameters', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('branch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Branch'])),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('comments', self.gf('django.db.models.fields.CharField')(max_length=140, null=True, blank=True)),
        ))
        db.send_create_signal(u'reports', ['BranchParameters'])

        # Adding unique constraint on 'BranchParameters', fields ['branch', 'key']
        db.create_unique(u'reports_branchparameters', ['branch_id', 'key'])

        # Adding model 'Connection'
        db.create_table(u'reports_connection', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('branch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Branch'])),
        ))
        db.send_create_signal(u'reports', ['Connection'])

        # Adding M2M table for field shared_with on 'Connection'
        m2m_table_name = db.shorten_name(u'reports_connection_shared_with')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('connection', models.ForeignKey(orm[u'reports.connection'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['connection_id', 'user_id'])

        # Adding model 'ConnectionParameters'
        db.create_table(u'reports_connectionparameters', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('connection', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Connection'])),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'reports', ['ConnectionParameters'])

        # Adding unique constraint on 'ConnectionParameters', fields ['connection', 'key']
        db.create_unique(u'reports_connectionparameters', ['connection_id', 'key'])

        # Adding model 'Bill'
        db.create_table(u'reports_bill', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('connection', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Connection'])),
            ('customer_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('customer_address', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('billing_date', self.gf('django.db.models.fields.DateField')()),
            ('amount', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=15, decimal_places=2)),
            ('dues', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=15, decimal_places=2)),
            ('due_date', self.gf('django.db.models.fields.DateField')()),
            ('after_dues', self.gf('django.db.models.fields.DecimalField')(default='0.00', max_digits=15, decimal_places=2)),
            ('pdf_url', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'reports', ['Bill'])


    def backwards(self, orm):
        # Removing unique constraint on 'ConnectionParameters', fields ['connection', 'key']
        db.delete_unique(u'reports_connectionparameters', ['connection_id', 'key'])

        # Removing unique constraint on 'BranchParameters', fields ['branch', 'key']
        db.delete_unique(u'reports_branchparameters', ['branch_id', 'key'])

        # Removing unique constraint on 'Branch', fields ['city', 'company']
        db.delete_unique(u'reports_branch', ['city_id', 'company_id'])

        # Deleting model 'City'
        db.delete_table(u'reports_city')

        # Deleting model 'Company'
        db.delete_table(u'reports_company')

        # Deleting model 'Branch'
        db.delete_table(u'reports_branch')

        # Deleting model 'BranchParameters'
        db.delete_table(u'reports_branchparameters')

        # Deleting model 'Connection'
        db.delete_table(u'reports_connection')

        # Removing M2M table for field shared_with on 'Connection'
        db.delete_table(db.shorten_name(u'reports_connection_shared_with'))

        # Deleting model 'ConnectionParameters'
        db.delete_table(u'reports_connectionparameters')

        # Deleting model 'Bill'
        db.delete_table(u'reports_bill')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'reports.bill': {
            'Meta': {'object_name': 'Bill'},
            'after_dues': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '15', 'decimal_places': '2'}),
            'amount': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '15', 'decimal_places': '2'}),
            'billing_date': ('django.db.models.fields.DateField', [], {}),
            'connection': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Connection']"}),
            'customer_address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'customer_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'due_date': ('django.db.models.fields.DateField', [], {}),
            'dues': ('django.db.models.fields.DecimalField', [], {'default': "'0.00'", 'max_digits': '15', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pdf_url': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'reports.branch': {
            'Meta': {'unique_together': "(('city', 'company'),)", 'object_name': 'Branch'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.City']"}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Company']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'reports.branchparameters': {
            'Meta': {'unique_together': "(('branch', 'key'),)", 'object_name': 'BranchParameters'},
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Branch']"}),
            'comments': ('django.db.models.fields.CharField', [], {'max_length': '140', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'reports.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'long_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'reports.company': {
            'Meta': {'object_name': 'Company'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'long_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'reports.connection': {
            'Meta': {'object_name': 'Connection'},
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Branch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shared_with': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'viewers'", 'blank': 'True', 'to': u"orm['auth.User']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'reports.connectionparameters': {
            'Meta': {'unique_together': "(('connection', 'key'),)", 'object_name': 'ConnectionParameters'},
            'connection': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Connection']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['reports']