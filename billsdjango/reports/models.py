from decimal import Decimal
from dateutil.relativedelta import relativedelta

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.validators import URLValidator


class ReportModel(models.Model):
     created_on = models.DateTimeField(auto_now_add=True, default=timezone.now)

     class Meta:
         abstract = True


class City(ReportModel):
    short_name = models.CharField(max_length=20) # khi, isb, lhr, rwl
    long_name = models.CharField(max_length=50) # karachi, islamabad, lahore, rawalpindi

    def __unicode__(self):
        return '%s (%s)' % (self.long_name.title(), self.short_name.upper())

    class Meta:
        verbose_name_plural = "Cities"


class Company(ReportModel):
    short_name = models.CharField(max_length=20) # kesc, ptcl, ssgc, kwsb etc
    long_name = models.CharField(max_length=100) # karachi electric supply company, pakistan telecommunication company limited, etc
    url = models.CharField(max_length=255)

    def __unicode__(self):
        return '%s (%s)' % (self.long_name.title(), self.short_name.upper())

    class Meta:
        verbose_name_plural = "Companies"


class Branch(ReportModel):
    city = models.ForeignKey(City)
    company = models.ForeignKey(Company)
    example_image = models.TextField(null=True, blank=True, validators=[URLValidator()])

    def __unicode__(self):
        return '%s (%s) branch of %s (%s)' % (self.company.short_name.upper(), self.company.pk, self.city.long_name, self.city.pk)

    class Meta:
        verbose_name_plural = "Branches"
        unique_together = (
            ('city', 'company'),
        )


class BranchParameters(ReportModel):
    branch = models.ForeignKey(Branch)
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=255)
    comments = models.CharField(max_length=140, blank=True, null=True)

    def __unicode__(self):
        return '%s (%s) of branch %s' % (self.value, self.key, self.branch)

    class Meta:
        verbose_name_plural = "Branch Parameters"
        unique_together = (
            ('branch', 'key'),
        )

class Connection(ReportModel):
    user = models.ForeignKey(User)
    branch = models.ForeignKey(Branch)
    shared_with = models.ManyToManyField(User, related_name='viewers', blank=True)

    def __unicode__(self):
        return '%s connection (id:%s) of %s' % (self.branch.company.short_name.upper(), self.pk, self.user.first_name or self.user.username)

    class Meta:
        verbose_name_plural = "Connections"


class ConnectionParameters(ReportModel):
    connection = models.ForeignKey(Connection)
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Connection Parameters"
        unique_together = (
            ('connection', 'key'),
        )


class Bill(ReportModel):
    connection = models.ForeignKey(Connection)
    customer_name = models.CharField(max_length=100)
    customer_address = models.CharField(max_length=100, blank=True, null=True)
    billing_date = models.DateField()
    amount = models.DecimalField(max_digits=15, decimal_places=2, default=Decimal('0.00'))
    dues = models.DecimalField(max_digits=15, decimal_places=2, default=Decimal('0.00'))
    due_date = models.DateField()
    after_dues = models.DecimalField(max_digits=15, decimal_places=2, default=Decimal('0.00'))
    pdf_url = models.CharField(max_length=255)

    @property
    def prev(self):
        prev_date = self.billing_date - relativedelta(months=1)
        last_month_bill = Bill.objects.filter(connection=self.connection,
                                                billing_date__year=prev_date.year,
                                                billing_date__month=prev_date.month) \
                                        .latest('pk')
        return last_month_bill

    @property
    def amount_change(self):
        try:
            return self.amount - self.prev.amount
        except:
            return ''

    @property
    def amount_percent_change(self):
        try:
            return self.amount_change / self.prev.amount * 100
        except:
            return ''

    def __unicode__(self):
        return '%s bill for %s' % (self.billing_date.strftime('%b %Y'), self.connection)

    class Meta:
        verbose_name_plural = "Bills"


class Units(ReportModel):
    bill = models.OneToOneField(Bill)
    previous_units_date = models.DateField(null=True, blank=True)
    previous_units = models.FloatField()
    current_units_date = models.DateField(null=True, blank=True)
    current_units = models.FloatField()
    adjusted_units = models.FloatField(default=0)
    billable_units = models.FloatField()

    @property
    def prev(self):
        prev_date = self.bill.billing_date - relativedelta(months=1)
        last_month_units = Units.objects.filter(bill__connection=self.bill.connection,
                                                bill__billing_date__year=prev_date.year,
                                                bill__billing_date__month=prev_date.month) \
                                        .latest('pk')
        return last_month_units

    @property
    def change(self):
        try:
            return self.billable_units - self.prev.billable_units
        except:
            return ''

    @property
    def percent_change(self):
        try:
            return self.change / self.prev.billable_units * 100
        except:
            return ''

    @property
    def billable_units_computed(self):
        try:
            return self.current_units - self.previous_units
        except:
            return self.billable_units

    @property
    def adjusted_units_computed(self):
        try:
            return self.billable_units - self.billable_units_computed
        except:
            return self.adjusted_units

    def __unicode__(self):
        return '%s units for %s' % (self.billable_units, self.bill)

    @property
    def price(self):
        expense = self.bill.amount or self.bill.dues or 0
        if self.billable_units <= 0:
            return ''
        return float(expense) / float(self.billable_units)

    @property
    def price_change(self):
        if self.billable_units <= 0 or self.prev.billable_units <= 0:
            return ''
        try:
            prev_expense = self.prev.bill.amount or self.prev.bill.dues or 0
            prev_price = float(prev_expense) / float(self.prev.billable_units)
            return self.price - prev_price
        except:
            return ''

    @property
    def price_percent_change(self):
        try:
            prev_expense = self.prev.bill.amount or self.prev.bill.dues or 0
            return float(self.price_change) / float(prev_expense) * 100
        except:
            return ''

    class Meta:
        verbose_name_plural = "Units"
