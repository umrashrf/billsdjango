import re

from itertools import chain

from dateutil.relativedelta import relativedelta

import django
from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.forms.formsets import formset_factory

from .. import forms
from .. import get_utc5_now, LoginRequiredMixin
from .. import models


class DashboardView(LoginRequiredMixin, TemplateView):
    title = 'Dashboard'
    template_name = 'dashboard.html'

    def post(self, request, *args, **kwargs):
        if request.user.is_staff:
            theuser = User.objects.get(username=self.request.POST.get('username'))
            theuser.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, theuser)
        return HttpResponseRedirect(reverse('dashboard'))

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)

        month = year = None
        r_date = self.request.GET.get('date')
        if r_date and re.match(r'^\d{2}-\d{4}$', r_date): # validate input :)
            month, year = r_date.split('-')

        user = User.objects.get(id=self.request.user.id)
        connections = models.Connection.objects.filter(Q(user=user) | Q(shared_with=user)).distinct()

        current_billing_date = get_utc5_now() - relativedelta(months=1)

        bills = models.Bill.objects.filter(Q(connection__user=user) | Q(connection__shared_with=user))
        if bills:
            latest_bill = bills.latest('billing_date')
            if latest_bill:
                current_billing_date = latest_bill.billing_date

        current_billing_year = year or current_billing_date.strftime('%Y')
        current_billing_month = month or current_billing_date.strftime('%m')

        allbills = {}
        total_dues = 0

        for connection in connections:
            name = connection.branch.company.short_name
            if name not in allbills:
                allbills[name] = {}
                allbills[name]['bills'] = []
            bills = (models.Bill.objects.filter(connection=connection,
                                                billing_date__year=current_billing_year,
                                                billing_date__month=current_billing_month)
                                        .order_by('-billing_date'))
            if bills:
                for bill in bills:
                    allbills[name]['bills'].append(bill)
                    total_dues += bill.dues

                latest_bill = (models.Bill.objects.filter(connection=connection)
                                                    .latest('billing_date'))
                allbills[name]['latest'] = latest_bill

        context['r_date'] = r_date
        context['range'] = [current_billing_date - relativedelta(months=m)
                            for m in range(0, 11)]
        context['allbills'] = allbills.iteritems()
        context['total_dues'] = total_dues
        context['title'] = self.title
        context['connections_count'] = len(connections)

        # only for staff users
        if self.request.user.is_staff:
            context['allusers'] = User.objects.all()
            context['currentuser'] = self.request.user
        return context


class ConnectionsList(LoginRequiredMixin, TemplateView):
    title = 'Your Connections'
    template_name = 'connection_list.html'

    def get_context_data(self, **kwargs):
        context = super(ConnectionsList, self).get_context_data(**kwargs)

        allconnections = {}
        user = User.objects.get(id=self.request.user.id)
        connections = models.Connection.objects.filter(Q(user=user) | Q(shared_with=user)).distinct()
        for connection in connections:
            company_name = connection.branch.company.short_name
            bparams = models.BranchParameters.objects.filter(branch=connection.branch)
            if len(bparams) < 2:
                bparams = list(bparams)
                bparams.append(models.BranchParameters.objects.none())
            if company_name not in allconnections:
                allconnections[company_name] = {
                    'params': bparams,
                    'connections': []
                }
            cparams = models.ConnectionParameters.objects.filter(connection=connection)
            if len(cparams) < 2:
                cparams = list(cparams)
                cparams.append(models.ConnectionParameters.objects.none())
            allconnections[company_name]['connections'].append({
                'connection': connection,
                'params': cparams,
            })

        context['title'] = self.title
        context['total_conn'] = len(allconnections)
        context['allconnections'] = allconnections.iteritems()
        return context


class AddConnection(LoginRequiredMixin, FormView):
    title = 'Add New Bill Connection'
    template_name = 'add_connection.html'
    form_class = forms.AddConnection

    def form_valid(self, form):
        return HttpResponseRedirect(reverse('add_company_connection',
                                        args=[form.cleaned_data['branch']]))


class AddCompanyConnection(LoginRequiredMixin, FormView):
    template_name = 'add_company_connection.html'
    form_class = forms.AddCompanyConnection

    def get_context_data(self, *args, **kwargs):
        context = super(AddCompanyConnection, self).get_context_data(*args, **kwargs)
        branch = models.Branch.objects.get(pk=self.kwargs.get('branch'))
        context['branch'] = branch
        context['company'] = branch.company
        context['request'] = self.request
        return context

    def get_form_kwargs(self):
        kwargs = super(AddCompanyConnection, self).get_form_kwargs()
        kwargs['branch'] = self.kwargs.get('branch')
        return kwargs

    def form_valid(self, form):
        branch = models.Branch.objects.get(pk=self.kwargs.get('branch'))
        branch_params = models.BranchParameters.objects.filter(branch=branch)
        conn = models.Connection.objects.create(branch=branch,
                                                user=self.request.user)
        for bparam in branch_params:
            value = form.cleaned_data.get(bparam.key)
            models.ConnectionParameters.objects.create(connection=conn,
                                                        key=bparam.key,
                                                        value=value)
        return HttpResponseRedirect(reverse('connections'))


class ShareConnection(LoginRequiredMixin, FormView):
    title = 'Share Connection'
    template_name = 'share_connection.html'
    form_class = forms.ShareConnection

    def form_valid(self, form):
        user_email = form.cleaned_data['email']
        try:
            user = User.objects.get(email=user_email)
        except:
            user = None
            messages.add_message(self.request, messages.ERROR, 'No user found with this email.')

        connection = models.Connection.objects.get(pk=self.kwargs.pop('connection_id'))

        if user:
            action = form.data['action']
            if action == 'delete':
                connection.shared_with.remove(user)
            else:
                connection.shared_with.add(user)

        return HttpResponseRedirect(self.request.path)

    def get_context_data(self, **kwargs):
        context = super(ShareConnection, self).get_context_data(**kwargs)
        connection = models.Connection.objects.get(pk=self.kwargs.pop('connection_id'))
        if connection:
            context['partners'] = connection.shared_with.all()
        return context


class ViewBillDetails(LoginRequiredMixin, TemplateView):
    template_name = 'bill_details.html'

    def get_context_data(self, bill_id=None, **kwargs):
        context = super(ViewBillDetails, self).get_context_data(**kwargs)

        selected_bill = None

        bill = models.Bill.objects.get(pk=bill_id)
        if bill.connection.user.id == self.request.user.id:
            selected_bill = bill
        elif bill.connection.shared_with.filter(pk=self.request.user.id).exists():
            selected_bill = bill

        try:
            latest_bill = (models.Bill.objects.filter(connection=selected_bill.connection)
                                                .latest('billing_date'))
        except:
            latest_bill = None

        last_year_bills = []
        if latest_bill:
            end_date = latest_bill.billing_date
            start_date = end_date - relativedelta(months=12)
            last_year_bills = (models.Bill.objects.filter(connection=selected_bill.connection,
                                                            billing_date__range=[start_date, end_date])
                                                    .order_by('-billing_date'))

        context['title'] = '%s (%s)' % (selected_bill.connection.branch.company.short_name,
                                            selected_bill.customer_name)
        context['chart_name'] = selected_bill.connection.branch.company.short_name
        context['selected_bill'] = selected_bill
        context['last_year_bills'] = last_year_bills
        return context

