from dateutil.relativedelta import relativedelta

from django.db.models import Q
from django.contrib.auth.models import User
from django.views.generic import TemplateView

from .. import get_utc5_now, LoginRequiredMixin
from .. import models


class Dashboard(LoginRequiredMixin, TemplateView):
    title = 'Unit Consumption'
    template_name = 'units/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)

        month = year = None
        r_date = self.request.GET.get('date')
        if r_date and re.match(r'^\d{2}-\d{4}$', r_date): # validate input :)
            month, year = r_date.split('-')

        user = User.objects.get(id=self.request.user.id)
        companies = models.Company.objects.filter(short_name__in=['k-electric', 'ssgc'])
        connections = models.Connection.objects.filter(Q(branch__company__in=companies),
                                                         Q(user=user) | Q(shared_with=user)) \
                                                .distinct()

        current_billing_date = get_utc5_now() - relativedelta(months=1)

        bills = models.Bill.objects.filter(Q(connection__branch__company__in=companies),
                                            Q(connection__user=user) | Q(connection__shared_with=user))
        if bills:
            latest_bill = bills.latest('billing_date')
            if latest_bill:
                current_billing_date = latest_bill.billing_date

        current_billing_year = year or current_billing_date.strftime('%Y')
        current_billing_month = month or current_billing_date.strftime('%m')

        allbills = {}

        for connection in connections:
            name = connection.branch.company.short_name
            if name not in allbills:
                allbills[name] = []
            bills = (models.Bill.objects.filter(connection=connection,
                                                billing_date__year=current_billing_year,
                                                billing_date__month=current_billing_month)
                                        .order_by('-billing_date'))
            for bill in bills:
                if hasattr(bill, 'units'):
                    allbills[name].append(bill)

        context['r_date'] = r_date
        context['range'] = [current_billing_date - relativedelta(months=m) for m in range(0, 11)]
        context['allbills'] = allbills.iteritems()
        context['title'] = self.title
        return context


class BillUnits(LoginRequiredMixin, TemplateView):
    title = 'Unit Consumption'
    template_name = 'units/bill.html'

    def get_context_data(self, bill_id=None, **kwargs):
        context = super(BillUnits, self).get_context_data(**kwargs)

        selected_bill = None

        bill = models.Bill.objects.get(pk=bill_id)
        if bill.connection.user.id == self.request.user.id:
            selected_bill = bill
        elif bill.connection.shared_with.filter(pk=self.request.user.id).exists():
            selected_bill = bill

        if selected_bill.connection.branch.company.short_name.lower() not in ['k-electric', 'ssgc']:
            return

        try:
            latest_bill = (models.Bill.objects.filter(connection=selected_bill.connection)
                                                .latest('billing_date'))
        except:
            latest_bill = None

        last_year_bills = []
        if latest_bill:
            end_date = latest_bill.billing_date
            start_date = end_date - relativedelta(months=12)
            last_year_bills = (models.Bill.objects.filter(connection=selected_bill.connection,
                                                            billing_date__range=[start_date, end_date])
                                                    .order_by('-billing_date'))

        context['title'] = '%s Units (%s)' % (selected_bill.connection.branch.company.short_name,
                                            selected_bill.customer_name)
        context['chart_name'] = selected_bill.connection.branch.company.short_name
        context['selected_bill'] = selected_bill
        context['last_year_bills'] = last_year_bills
        return context
