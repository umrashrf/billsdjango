from django.db.models import Q
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse

from .. import LoginRequiredMixin
from .. import get_utc5_now
from .. import models


class ChangesView(LoginRequiredMixin, TemplateView):
    template_name = 'changes.html'

    def get_context_data(self, **kwargs):
        context = super(ChangesView, self).get_context_data(**kwargs)

        user = User.objects.get(id=self.request.user.id)
        conns = models.Connection.objects.filter(Q(user=user) | Q(shared_with=user)).distinct()

        if conns:
            conn_id = kwargs.get('conn_id', None)
            if not conn_id:
                conn_id = conns.latest('created_on').id

            conn_info = []
            try:
                connection = conns.get(id=conn_id)
            except:
                raise Http404
            conn_info.append({
                'key': 'Id',
                'value': connection.id
            })
            bparams = models.BranchParameters.objects.filter(branch=connection.branch)
            cparams = models.ConnectionParameters.objects.filter(connection=connection)
            for bparam in bparams:
                for cparam in cparams:
                    if bparam.key == cparam.key:
                        conn_info.append({
                            'key': bparam.value,
                            'value': cparam.value
                        })

            last_year_bills = (models.Bill.objects.filter(connection=connection)
                                                  .order_by('-billing_date')[:12])
            context['connections'] = conns
            context['connection_info'] = conn_info
            context['bills'] = last_year_bills
            if last_year_bills:
                context['title'] = '%s (%s) Connection' % (
                                    connection.branch.company.short_name,
                                    last_year_bills[0].customer_name)
            else:
                context['title'] = '%s (%s) Connection' % (
                                    connection.branch.company.short_name,
                                    'Updating...')
        else:
            context['title'] = 'Consolidated View'
        return context
