from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'', include('billsdjango.reports.urls')),
    url(r'^accounts/', include('billsdjango.accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
