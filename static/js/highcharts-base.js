function HighchartsBase(target) {
    this.args = {
        title: {
            text: ''
        },
        legend: {
            layout: 'horizontal',
            align: 'right',
            verticalAlign: 'top',
            floating: true,
            backgroundColor: '#FFFFFF'
        },
        chart: {
            type: 'line',
            marginTop: 75
        },
        credits: {
            enabled: false
        },
    };

    this.render = function(data) {
        $.extend(this.args, {
            series: data
        })
        $(target).highcharts(this.args);
    };
}
