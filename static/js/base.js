function Charts() {
    this.suffix = '/- Rs.';
    this.getSuffix = function() {
        return this.suffix;
    };

    this.renderAll = function() {
        var self = this,
            charts = document.getElementsByClassName('chart');
        for (var c = 0; c < charts.length; c++) {
          url = charts[c].getAttribute('data-chart-url');
          chartName = charts[c].getAttribute('data-chart-name');
          chartColor = charts[c].getAttribute('data-chart-color');

          ajax.get(url, null, function(data) {
            data = JSON.parse(data);

            var chart = new HighchartsBase('.chart');
            $.extend(chart.args, {
                tooltip: {
                    valueSuffix: self.getSuffix(),
                    shared: true,
                    crosshairs: true
                },
                xAxis: {
                    type: 'datetime',
                    title: {
                        text: 'Date (M, Y)'
                    },
                    gridLineWidth: 1,
                },
                yAxis: {
                    title: {
                        text: 'Rs. (PKR)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                }
            });
            chart.render(data);
          });
        }
    };
}
