LOCAL = False

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.mysql',
        'NAME': 'bills',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}
