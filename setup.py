#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='Bills',
    version='1.0',
    description='Utility bills aggregator and manager',
    long_description=open('README.md').read(),
    author='Bills Team',
    maintainer='Umair Ashraf',
    maintainer_email='umair@bills.pk',
    url='http://bills.pk/',
    install_requires=[
        'Django==1.6.2',
        'django-registration==1.0',
        'python-dateutil==2.2',
        'MySQL-python==1.2.5',
        'South==0.8.4',
        'boto==2.32.1',
        'pytz==2014.7',
        'uWSGI==2.0.8',
        'scrapinghub',
        'django-mailer'
    ],
    dependency_links=[
        'git+https://github.com/scrapinghub/python-scrapinghub.git@1.6.0#egg=scrapinghub',
        'git+https://github.com/umrashrf/django-mailer.git'
    ],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    scripts=[
        'bin/export-connections.py',
        'bin/import-bills.py',
        'bin/delete-pdfs.py'
    ]
)
