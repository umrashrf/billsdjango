#!/usr/bin/env python

import os
import sys
import json
import logging
import urllib2
import smtplib
import subprocess

from decimal import Decimal
from datetime import datetime

from subprocess import call
from scrapinghub import Connection as ShubConnection

from django.db.models import Q
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.contrib.contenttypes.models import ContentType

from billsdjango import settings
from reports import models


class FileImporter(object):
    fields = {
        'customer_name': 'customer_name',
        'customer_address': 'customer_address',
        'billing_date': 'billing_date',
        'amount': 'amount',
        'dues': 'dues',
        'due_date': 'due_date',
        'after_dues': 'after_dues',
        'pdf_urls': 'pdf_url',
        'city': 'city',
        'company': 'company',
        'connection': 'connection',
        'user': 'user'
    }

    required_fields = ('connection', 'billing_date',)

    companies = []

    def import_file(self, filepath='fixtures/bills.jl'):
        with open(filepath, 'r') as f:
            lines = f.read().splitlines()
            for line in lines:
                item = json.loads(line)
                conn = self.get_connection(item)
                self.import_item(conn, self.clean_item(conn, item))

    def clean_item(self, conn, item):
        item.pop('city')
        if conn:
            item['connection'] = conn.pk
        return item

    def import_item(self, conn, item):
        data = {}
        for fname, fvalue in self.fields.iteritems():
            if fvalue in item:
                data[fvalue] = item[fvalue]

        try:
            connection = models.Connection.objects.get(pk=item.get('connection'))
        except:
            return # add connection before importing bills for it

        data['connection'] = connection
        models.Bill.objects.get_or_create(billing_date=item['billing_date'],
                                            connection=connection,
                                            defaults=data)

    def get_connection(self, item):
        if item.pop('company') not in self.companies:
            return None # item is not for this importer
        Q_object = Q()
        bparams = models.BranchParameters.objects.filter(branch__company__short_name__in=self.companies)
        for bp in bparams:
            Q_object.add(Q(key=bp.key, value=item.pop(bp.key)), Q.OR)
        Q_object.add(Q(connection__user__pk=item.pop('user')), Q.AND)
        cparams = models.ConnectionParameters.objects.filter(Q_object)
        if len(bparams) == len(cparams):
            return cparams[0].connection
        return None


class PTCLImporter(FileImporter):
    companies = ['ptcl']


class KESCImporter(FileImporter):
    companies = ['k-electric']


class SSGCImporter(FileImporter):
    companies = ['ssgc']


class KWSBImporter(FileImporter):
    companies = ['kwsb']


def main():

    global log

    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)

    importers = [
        PTCLImporter,
        SSGCImporter,
        KWSBImporter,
        KESCImporter,
    ]

    for i in importers:
        importer = i()
        importer.import_file()


if __name__ == '__main__':
    main()
