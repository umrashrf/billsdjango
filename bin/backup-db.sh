#!/bin/bash

filename=bills_$(date +%s).sql
mysqldump -u root -p$1 bills > $filename
s3cmd put $filename s3://billsscrapy/backups/
rm $filename
