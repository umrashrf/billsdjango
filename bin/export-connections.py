#!/usr/bin/env python

import os
import json

from subprocess import call

from django.contrib.auth.models import User
from django.forms.models import model_to_dict

from billsdjango.reports import models


def get_bill_csv(branch):
    rv = []
    for conn in models.Connection.objects.filter(branch=branch):
        params = {
            'user': str(conn.user.id),
            'city': conn.branch.city.short_name,
            'company': conn.branch.company.short_name,
            'connection': conn.pk,
        }
        for cparam in models.ConnectionParameters.objects.filter(connection=conn):
            params.update({ cparam.key: cparam.value })
        rv.append(json.dumps(params, ensure_ascii=False))
    return '\n'.join(rv)

def upload_s3(filename):
    # XXX: use Boto
    os.system("s3cmd -c /home/billspk/.s3cfg put -P %s s3://billsscrapy/" % filename)

def main():
    for branch in models.Branch.objects.all():
        filename = '%s_%s.jl' % (branch.company.short_name.lower(),
                                 branch.city.short_name.lower())
        with file(filename, 'w+') as f:
            f.write(get_bill_csv(branch))
        upload_s3(filename)

if __name__ == '__main__':
    main()
