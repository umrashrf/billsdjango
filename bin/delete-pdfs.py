#!/usr/bin/env python

'''
We need to delete PDFs in billsscrapy/users_bills/full/
which are not stored in MySQL database
and which are older than 2 weeks
'''

import sys
import boto
import time
import logging

from datetime import datetime, timedelta

from billsdjango import settings
from billsdjango.reports import models


def main():
    conn = boto.connect_s3('AKIAIT5KPLENBSASUQQA', 'MJE60h2uJYEp2ebPtn5OABi/lLHsE8p5HMGTV3hP')
    bucket = conn.get_bucket('billsscrapy')
    del_count = 0
    for key in bucket.list(prefix='users_bills'):
        last_modified_ts = time.strptime(key.last_modified[:19], "%Y-%m-%dT%H:%M:%S")
        last_modified_dt = datetime.fromtimestamp(time.mktime(last_modified_ts))
        if last_modified_dt <= (datetime.now() - timedelta(days=14)):
            path = key.name.encode('utf-8')
            url = '%s/%s' % (settings.FILES_URL_PREFIX, path.lstrip('users_bills/'))
            if not models.Bill.objects.filter(pdf_url=url).count():
                bucket.delete_key(path)
                del_count += 1
                log.info('Deleted %s' % path)
    log.info('Deleted %d out of %d pdf from S3' % (del_count, models.Bill.objects.count()))


if __name__ == '__main__':
    global log

    log = logging.getLogger()
    log.setLevel(logging.INFO)
    ch = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)

    main()
