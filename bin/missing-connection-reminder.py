#!/usr/bin/env python


from django.db.models import Q
from django.contrib.auth.models import User

from billsdjango import settings
from reports.models import Branch, Connection

if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_mail
else:
    from django.core.mail import send_mail


def send_reminder_to_add_connections(user):
    data = dict(
        username=user.first_name or user.username,
    )
    subject = 'Add some connections'
    message = ''
    tpl = '%s/%s' % (settings.BASE_DIR, 'reports/templates/emails/no_connection_reminder.txt')
    with open(tpl, 'r') as f:
        message = f.read()
    send_mail(subject, message % data, settings.DEFAULT_FROM_EMAIL,
                [user.email], fail_silently=True)

def send_reminder_to_add_more_connections(user):
    data = dict(
        username=user.first_name or user.username,
    )
    subject = 'Add more connections'
    message = ''
    tpl = '%s/%s' % (settings.BASE_DIR, 'reports/templates/emails/missing_connection_reminder.txt')
    with open(tpl, 'r') as f:
        message = f.read()
    send_mail(subject, message % data, settings.DEFAULT_FROM_EMAIL,
                [user.email], fail_silently=True)

def main():
    for user in User.objects.all():
        # get random connection and check if user has any connections
        user_conns = Connection.objects.filter(Q(user=user) | Q(shared_with=user)).distinct()
        if not user_conns:
            # user has no connection
            print 'Sending no connections reminder to %s <%s>' % (user.username, user.email)
            send_reminder_to_add_connections(user)
            continue

        # get user city by random connection of his/her
        random_conn = user_conns.latest('pk')
        random_city = random_conn.branch.city

        # get all branches in that city
        all_branches = Branch.objects.filter(city=random_city).values_list('pk', flat=True)

        # get all branches in which user has connections
        user_branches = [c.branch.pk for c in user_conns]

        missing_branches = list(set(all_branches) - set(user_branches))
        if missing_branches:
            print 'Sending missing connections reminder to %s <%s>' % (user.username, user.email)
            send_reminder_to_add_more_connections(user)


if __name__ == '__main__':
    main()
