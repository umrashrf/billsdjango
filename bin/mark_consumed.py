#!/usr/bin/env python

from scrapinghub import Connection as ShubConnection

from billsdjango.settings import DASH_API_KEY, DASH_PROJECT_ID, CONSUMED_TAG


def main():
    conn = ShubConnection(DASH_API_KEY)
    project = conn[DASH_PROJECT_ID]
    jobs = project.jobs(state='finished', lacks_tag=CONSUMED_TAG)
    print jobs.count()
    for job in jobs:
        job.update(add_tag=CONSUMED_TAG)
    print jobs.count()

if __name__ == '__main__':
    main()