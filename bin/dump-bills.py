#!/usr/bin/env python

import json

from django.forms.models import model_to_dict

from reports.models import PTCLModel, KESCModel, SSGCModel, KWSBModel


def get_all_bills():
    extras = {
        0: 'k-electric',
        1: 'kwsb',
        2: 'ssgc',
        3: 'ptcl',
    }

    bill_models = [KESCModel, KWSBModel, SSGCModel, PTCLModel]
    for i, bill_model in enumerate(bill_models):
        for conn in bill_model.objects.all():
            citem = model_to_dict(conn)
            for bill in conn.bills.all():
                bitem = model_to_dict(bill)
                bill_id = bitem.pop('bill_id')
                bill_type = bitem.pop('bill_type')
                item = dict(citem.items() + bitem.items())
                for f in item:
                    item[f] = str(item[f])
                item['city'] = 'khi'
                item['company'] = extras[i]
                yield json.dumps(item)

def main():
    with file('fixtures/bills.jl', 'w+') as f:
        f.write('\n'.join(get_all_bills()))

if __name__ == '__main__':
    main()
