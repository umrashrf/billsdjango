#!/usr/bin/env python

import subprocess

from datetime import datetime

from django.core.mail import send_mail

from billsdjango import settings
from reports import models


def test_kwsb():
    fields = {
        'customer_name': '39,178,308,55',
        'customer_address': '39,178,308,55',
        'billing_month': '475,160,97,17',
        'amount': 'amount',
        'dues': '664,292,98,18',
        'due_date': '664,160,98,18',
        'after_dues': '663,329,98,18',
        'pdf_urls': 'pdf_url',
        'connection': 'connection',
    }

    data = {}
    pdf_file = '/home/umair/Development/Bills/billsdjango/downloads/kesc/KESCv2 Sept 2014.pdf'
    for fname, fvalue in fields.iteritems():
        if fvalue.count(',') > 2:
            x, y, w, h = fvalue.split(',')
            output = subprocess.check_output(
                        "pdftotext -q -x %s -y %s -W %s -H %s -r 100 -layout \"%s\" -" % (x, y, w, h, pdf_file),
                            shell=True)
            data[fvalue] = output
    print data

def test_kesc():
    fields = {
        'customer_name': '112,69,300,50',
        'customer_address': '112,69,300,50',
        'billing_date': 'billing_date',
        'amount': 'amount',
        'dues': 'dues',
        'due_date': 'due_date',
        'after_dues': 'after_dues',
        'pdf_urls': 'pdf_url',
        'connection': 'connection',
    }

    data = {}
    pdf_file = '/home/umair/Development/Bills/billsdjango/downloads/kesc/KESCv2 Sept 2014.pdf'
    for fname, fvalue in fields.iteritems():
        if fvalue.count(',') > 2:
            x, y, w, h = fvalue.split(',')
            output = subprocess.check_output(
                        "pdftotext -q -x %s -y %s -W %s -H %s -r 100 -layout \"%s\" -" % (x, y, w, h, pdf_file),
                            shell=True)
            data[fvalue] = output
    print data

def test_email():
    bill = models.Bill.objects.filter(connection__user__username='umair').latest('pk')

    item = dict(
        company=bill.connection.branch.company.short_name,
        username=bill.connection.user.first_name or bill.connection.user.username,
        billing_date=bill.billing_date.strftime('%b %Y'),
        dues=bill.dues,
        due_date=bill.due_date.strftime('%d %b %Y'),
        connection_id=bill.connection.id
    )

    subject = 'New %(company)s Bill Available at Bills.pk' % item

    message = ''
    tpl = '%s/%s' % (settings.BASE_DIR, 'reports/templates/emails/new_bill_alert.txt')
    with open(tpl, 'r') as f:
        message = f.read()
        message = message % item

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [bill.connection.user.email],
                fail_silently=False)

def main():
    # test_kwsb()
    # test_kesc()
    test_email()

if __name__ == '__main__':
    main()
