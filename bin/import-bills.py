#!/usr/bin/env python

import os
import sys
import json
import logging
import urllib2
import subprocess

from decimal import Decimal
from datetime import datetime as thedatetime
from dateutil.relativedelta import relativedelta

from subprocess import call
from scrapinghub import Connection as ShubConnection

from django.db.models import Q
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.contrib.contenttypes.models import ContentType

from billsdjango import settings
from billsdjango.reports import models
from billsdjango.reports import *

if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_mail
else:
    from django.core.mail import send_mail


class DashImporter(object):
    fields = {
        'customer_name': 'customer_name',
        'customer_address': 'customer_address',
        'billing_date': 'billing_date',
        'amount': 'amount',
        'dues': 'dues',
        'due_date': 'due_date',
        'after_dues': 'after_dues',
        'pdf_urls': 'pdf_url',
        'connection': 'connection'
    }

    required_fields = ('connection', 'billing_date',)

    companies = []

    def __init__(self):
        conn = ShubConnection(settings.DASH_API_KEY)
        self.project = conn[settings.DASH_PROJECT_ID]

    def import_jobs(self, spider):
        jobs = self.project.jobs(spider=spider, state='finished', lacks_tag=settings.CONSUMED_TAG)
        for job in jobs:
            self.import_job(job)

    def get_job(self, jobid):
        return self.project.job(id=jobid)

    def import_job(self, job, override=False):
        logging.info('Importing job %s' % job.id)
        date = job.info['started_time'][0:10]
        for item in job.items():
            cleaned_item = self.clean_item(item)
            if all([True if f in cleaned_item else False for f in self.required_fields]):
                self.import_item(cleaned_item, job, date, override=override)
        job.update(add_tag=settings.CONSUMED_TAG)

    def clean_item(self, item):
        for fname, fvalue in self.fields.iteritems():
            if fname == 'pdf_urls':
                for pdf_url in item[fname]:
                    item[fvalue] = '%s/%s' % (settings.FILES_URL_PREFIX, pdf_url['path'])
                    item.pop(fname)
                    break
        return item

    def import_item(self, item, job, date, override=False):
        data = {}
        for fname, fvalue in self.fields.iteritems():
            if fvalue in item:
                data[fvalue] = item[fvalue]

        pdf_downloaded = False
        if hasattr(self, 'download_pdf'):
            pdf_downloaded = self.download_pdf(item)

        if hasattr(self, 'get_extras') and pdf_downloaded:
            extras = self.get_extras(item)
            data.update(extras)

        try:
            connection = models.Connection.objects.get(pk=item.pop('connection'))
        except:
            return # add connection before importing bills for it

        data['connection'] = connection
        bill, created = models.Bill.objects.get_or_create(billing_date=data['billing_date'],
                                                            connection=data['connection'],
                                                            defaults=data)
        if hasattr(self, 'post_process') and pdf_downloaded:
            self.post_process(item, bill)

        if hasattr(self, 'remove_pdf') and pdf_downloaded:
            self.remove_pdf()

        if created:
            logging.info('Bill created for %s' % bill.connection)
            # only send email of past 1 month old bills
            min_date = (get_utc5_now() - relativedelta(months=1)).replace(day=1)
            the_date = get_utc5_date(thedatetime.strptime(bill.billing_date, '%Y-%m-%d')).replace(day=1)
            if the_date.date() >= min_date.date():
                self.send_alert_email(bill)
        elif override:
            for k, v in data.iteritems():
                if v:
                    setattr(bill, k, v)
            bill.save()

    def send_alert_email(self, bill):
        def format_date(dt, format='%b %Y'):
            return thedatetime.strptime(dt, '%Y-%m-%d').strftime(format)

        item = dict(
            email=bill.connection.user.email,
            company=bill.connection.branch.company.short_name,
            username=bill.connection.user.first_name or bill.connection.user.username,
            billing_date=format_date(bill.billing_date, format='%b %Y'),
            dues=bill.dues,
            due_date=format_date(bill.due_date, format='%d %b %Y'),
            bill_id=bill.id
        )

        subject = 'New %(company)s Bill Available at Bills.pk' % item

        message = ''
        tpl = '%s/%s' % (settings.BASE_DIR, 'billsdjango/reports/templates/emails/new_bill_alert.txt')
        with open(tpl, 'r') as f:
            message = f.read()

        logging.info('Sending notification for %(company)s bill to %(username)s' % item)
        send_mail(subject, message % item, settings.DEFAULT_FROM_EMAIL, [item.get('email')], fail_silently=True)

        for su in bill.connection.shared_with.all():
            item.update(email=su.email, username=su.first_name or su.username)
            logging.info('Sending notification for %(company)s bill to %(username)s' % item)
            send_mail(subject, message % item, settings.DEFAULT_FROM_EMAIL, [item.get('email')], fail_silently=True)


class PDFImporter(object):

    def download_pdf(self, item):
        pdf_dest = '/tmp'
        if 'pdf_url' in item and len(item['pdf_url'].strip()) > 0:
            pdf_url = item['pdf_url']
            try:
                res = urllib2.urlopen(pdf_url)
                content_type = res.info().getheader('Content-Type')
                if content_type not in ['application/pdf',
                                        'application/x-download',
                                        'application/octet-stream']:
                    log.exception('PDF content type (%s) is not in list.' % content_type)
                    return False
                content = res.read()
            except:
                return False
            filename = os.path.basename(pdf_url)
            pdf_file = '%s/%s' % (pdf_dest, filename)
            with open(pdf_file, 'wb') as pdf:
                pdf.write(content)
            if not os.path.exists(pdf_file):
                log.exception('PDF file not found. Either it is not downloaded or not saved (written).')
                return False
            self._pdf_file = pdf_file
        return True

    def get_extras(self, item):
        return {}

    def post_process(self, item, bill):
        pass

    def remove_pdf(self):
        if hasattr(self, '_pdf_file'):
            os.remove(self._pdf_file)
            del self._pdf_file


class UnitsImporter(PDFImporter):

    def post_process(self, item, bill):
        if hasattr(self, '_pdf_file'):
            self.import_units(item, bill)

    def import_units(self, item, bill):
        data = {}
        for fname, fvalue in self.unit_fields.iteritems():
            if fvalue.count(',') > 2:
                x, y, w, h = fvalue.split(',')
                output = subprocess.check_output(
                            "pdftotext -q -x %s -y %s -W %s -H %s -r 100 -layout %s -" % (x, y, w, h, self._pdf_file),
                                shell=True)
                data[fname] = getattr(self, 'parse_%s' % fname, lambda x: x)(output.strip())
        units, created = models.Units.objects.get_or_create(bill=bill, defaults=data)


class PTCLImporter(DashImporter):
    spiders = ['ptcl']
    companies = ['ptcl']


class KESCImporter(DashImporter):
    spiders = ['kesc']
    companies = ['k-electric']


class SSGCImporter(DashImporter, UnitsImporter):
    spiders = ['ssgc']
    companies = ['ssgc']

    unit_fields = {
        'current_units_date': '100,550,54,18',
        'current_units': '160,550,36,18',
        'previous_units_date': '200,550,56,18',
        'previous_units': '260,550,36,18',
        'billable_units': '320,550,80,18',
    }

    def clean_item(self, item):
        item = super(SSGCImporter, self).clean_item(item)
        for k in item:
            if k in ['dues', 'after_dues']:
                item[k] = item[k].replace('Rs.', '').strip()
        return item

    def parse_current_units_date(self, dt):
        return thedatetime.strptime(dt, '%d-%b-%Y').strftime('%Y-%m-%d')

    def parse_previous_units_date(self, dt):
        return thedatetime.strptime(dt, '%d-%b-%Y').strftime('%Y-%m-%d')


class KWSBImporter(DashImporter, PDFImporter):
    spiders = ['kwsb']
    companies = ['kwsb']

    fields = {
        'customer_name': '39,178,308,55',
        'customer_address': '39,178,308,55',
        'billing_month': '475,160,97,17',
        'amount': 'amount',
        'dues': '664,292,98,18',
        'due_date': '664,160,98,18',
        'after_dues': '663,329,98,18',
        'pdf_urls': 'pdf_url',
        'connection': 'connection',
    }

    def get_extras(self, item):
        if not hasattr(self, '_pdf_file'):
            return {}

        data = {}
        for fname, fvalue in self.fields.iteritems():
            if fvalue.count(',') > 2:
                x, y, w, h = fvalue.split(',')
                output = subprocess.check_output(
                            "pdftotext -q -x %s -y %s -W %s -H %s -r 100 -layout %s -" % (x, y, w, h, self._pdf_file),
                                shell=True)
                data[fname] = output
            elif fvalue in item:
                data[fvalue] = item[fvalue]

        for i, fname in enumerate(['customer_name', 'customer_address']):
            name_address = data[fname].split('\n', 1)
            data[fname] = name_address[i].strip()

        for fname in data:
            if isinstance(data[fname], str):
                data[fname] = data[fname].strip()

        data['billing_date'] = (thedatetime.strptime(data.pop('billing_month'), '%b,%Y')
                                        .strftime('%Y-%m-%d'))
        data['due_date'] = (thedatetime.strptime(data.get('due_date'), '%d/%m/%Y')
                                    .strftime('%Y-%m-%d'))
        return data


class KESC_V2Importer(DashImporter, UnitsImporter):
    spiders = ['kesc_v2']
    companies = ['k-electric']

    fields = {
        'customer_name': '112,69,300,50',
        'customer_address': '112,69,300,50',
        'billing_date': 'billing_date',
        'amount': 'amount',
        'dues': 'dues',
        'due_date': 'due_date',
        'after_dues': 'after_dues',
        'pdf_urls': 'pdf_url',
        'connection': 'connection',
    }

    unit_fields = {
        'current_units_date': '546,215,75,25',
        'current_units': '75,255,88,25',
        'previous_units': '164,255,93,25',
        'adjusted_units': '267,255,100,25',
        'billable_units': '484,255,60,25',
    }

    def get_extras(self, item):
        if not hasattr(self, '_pdf_file'):
            return {}

        data = {}
        for fname, fvalue in self.fields.iteritems():
            if fvalue.count(',') > 2:
                if not os.path.exists(self._pdf_file):
                    data[fname] = ''
                    continue
                x, y, w, h = fvalue.split(',')
                output = subprocess.check_output(
                            "pdftotext -q -x %s -y %s -W %s -H %s -r 100 -layout %s -" % (x, y, w, h, self._pdf_file),
                                shell=True)
                data[fname] = output
            elif fvalue in item:
                data[fvalue] = item[fvalue]

        for i, fname in enumerate(['customer_name', 'customer_address']):
            name_address = data[fname].split('\n', 1)
            data[fname] = name_address[i].strip()

        for fname in data:
            if isinstance(data[fname], str):
                data[fname] = data[fname].strip()

        return data

    def parse_current_units_date(self, dt):
        if dt:
            return thedatetime.strptime(dt, '%d-%b-%y').strftime('%Y-%m-%d')
        return dt

    def parse_adjusted_units(self, text):
        if not text:
            return '0'
        return text


def main():

    global log

    log = logging.getLogger()
    log.setLevel(logging.INFO)
    ch = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)

    importers = [
        PTCLImporter,
        SSGCImporter,
        KWSBImporter,
        KESC_V2Importer,
    ]

    def get_importer(spider):
        for importer in importers:
            if spider in importer.spiders:
                return importer

    if len(sys.argv) == 2:
        logging.info('Importing jobs for spider %s' % sys.argv[1])
        importer = get_importer(sys.argv[1])()
        importer.import_jobs(sys.argv[1])

    elif len(sys.argv) == 3:
        logging.info('Importing job %s for spider %s' % (sys.argv[1], sys.argv[2]))
        importer = get_importer(sys.argv[1])()
        job = importer.get_job(sys.argv[2])
        importer.import_job(job)

    elif len(sys.argv) == 4:
        importer = get_importer(sys.argv[1])()
        job = importer.get_job(sys.argv[2])
        importer.import_job(job, override=sys.argv[3])

    else:
        for i in importers:
            importer = i()
            for spider in importer.spiders:
                logging.info('Importing jobs for spider %s' % spider)
                importer.import_jobs(spider)


if __name__ == '__main__':
    main()
