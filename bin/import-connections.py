#!/usr/bin/env python

import sys
import json
import logging
import requests

from django.db.models import Q
from django.contrib.auth.models import User

from reports import models


class ConnectionsImporter():

    def import_json(self):
        response = requests.get(self.url)
        for line in response.text.splitlines():
            item = json.loads(line)
            self.import_item(item)

    def import_item(self, item):
        conn = self.get_connection(item) # connection by params
        if not conn:
            user = User.objects.get(pk=item.get('user'))
            branch = models.Branch.objects.get(pk=self.branch_pk)
            conn = models.Connection.objects.create(user=user,
                                                    branch=branch)
            for u in item.get('shared_with'):
                conn.shared_with.add(u)

            bparams = models.BranchParameters.objects.filter(branch=self.branch_pk)
            for bp in bparams:
                models.ConnectionParameters.objects.create(connection=conn,
                                                            key=bp.key,
                                                            value=item.get(bp.key))

    def get_connection(self, item):
        Q_object = Q()
        bparams = models.BranchParameters.objects.filter(branch=self.branch_pk)
        for bp in bparams:
            Q_object.add(Q(key=bp.key, value=item.get(bp.key)), Q.OR)
        Q_object.add(Q(connection__user__pk=item.get('user')), Q.AND)
        cparams = models.ConnectionParameters.objects.filter(Q_object)
        if len(bparams) == len(cparams):
            return cparams[0].connection
        return None


class KESCImporter(ConnectionsImporter):
    branch_pk = 1
    url = 'https://s3.amazonaws.com/billsscrapy/reports_kescmodel.jl'


class KWSBImporter(ConnectionsImporter):
    branch_pk = 2
    url = 'https://s3.amazonaws.com/billsscrapy/reports_kwsbmodel.jl'


class SSGCImporter(ConnectionsImporter):
    branch_pk = 3
    url = 'https://s3.amazonaws.com/billsscrapy/reports_ssgcmodel.jl'


class PTCLImporter(ConnectionsImporter):
    branch_pk = 4
    url = 'https://s3.amazonaws.com/billsscrapy/reports_ptclmodel.jl'


if __name__ == '__main__':
    global log

    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)

    importers = [
        KESCImporter,
        PTCLImporter,
        SSGCImporter,
        KWSBImporter,
    ]

    for i in importers:
        importer = i()
        importer.import_json()
