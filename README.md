## Very Important!

Please change **CONSUMED\_TAG** from **billsdjango/local_settings.py** to your custom string. 

I recommend setting it to:

"consumed\_[YOUR NAME]\_[YOUR COMPUTER NAME]"