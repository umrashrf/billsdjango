# how to make new prod build

* virtualenv billspk
* source billspk/bin/activate
* pip install git+ssh://git@bitbucket.org/umrashrf/billsdjango.git
* sudo vim /etc/bills/local_settings.py
* export PYTHONPATH=/etc/bills:/home/billspk/billspk
* export DJANGO_SETTINGS_MODULE=billsdjango.settings
* django-admin.py migrate
* sudo vim /etc/nginx/sites-available/billspk.conf
* sudo ln -s /etc/nginx/sites-available/bills.pk /etc/nginx/sites-enabled/billspk.conf
* cd /usr/share
* git archive --remote=ssh://git@bitbucket.org/umrashrf/billsdjango.git master static | tar xvf -
* setup cron file
