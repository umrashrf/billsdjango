# we will need to rebuild container if we change this file

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['bills.pk']

DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.mysql',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '3306',
    }
}

# beta mode has no registration open
REGISTRATION_OPEN = False

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_USE_TLS = 1
EMAIL_HOST_USER = ''
SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = ''
