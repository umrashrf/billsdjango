PYTHONPATH=/etc/bills:/home/billspk/billspk
DJANGO_SETTINGS_MODULE=billsdjango.settings
PATH=/home/billspk/billspk/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# FOR /etc/cron.d
# m h dom mon dow user command

0 3,9,15,21 * * * billspk export-connections.py > /home/billspk/log/export_connections_$(date '+\%Y-\%m-\%dT\%H-\%M-\%S').log 2>&1

0 4,10,16,22 * * * billspk import-bills.py kesc_v2 > /home/billspk/log/import_kesc_v2_$(date '+\%Y-\%m-\%dT\%H-\%M-\%S').log 2>&1
0 4,10,16,22 * * * billspk import-bills.py ssgc > /home/billspk/log/import_ssgc_$(date '+\%Y-\%m-\%dT\%H-\%M-\%S').log 2>&1
0 4,10,16,22 * * * billspk import-bills.py ptcl > /home/billspk/log/import_ptcl_$(date '+\%Y-\%m-\%dT\%H-\%M-\%S').log 2>&1
0 4,10,16,22 * * * billspk import-bills.py kwsb > /home/billspk/log/import_kwsb_$(date '+\%Y-\%m-\%dT\%H-\%M-\%S').log 2>&1

0 5,11,17,23 * * * billspk delete-pdfs.py | tee /home/billspk/log/delete_pdfs_$(date '+\%Y-\%m-\%dT\%H-\%M-\%S').log 2>&1

* * * * * billspk django-admin.py send_mail
0,20,40 * * * * billspk django-admin.py retry_deferred
